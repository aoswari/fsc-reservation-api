const ErrorResponse = require('../utils/errorResponse')
const Reservation = require('../models/Reservation');
// @desc       Get all reservations
// @route      GET /api/v1/reservations
// @access     Public
exports.getReservations = async (req, res, next) => {
    try {
        const reservations = await Reservation.find()
        res.status(200).json({ success: true, data: reservations })
    } catch (err) {
        res.status(400).json({ success: false });
    }
}

// @desc       Get single reservation
// @route      GET /api/v1/reservations/:id
// @access     Public
exports.getReservation = async (req, res, next) => {
    try {
        const reservation = await Reservation.findById(req.params.id)

        if (!reservation) {
            return new ErrorResponse(`Reservation not found with id of ${req.params.id}`, 400);
        }
        res.status(200).json({ success: true, data: reservation })
    } catch (err) {
        // res.status(400).json({ success: false });
        next(new ErrorResponse(`Reservation not found with id of ${req.params.id}`, 404));
    }
}

// @desc       Create new reservation
// @route      POST /api/v1/reservations
// @access     Private
exports.createReservation = async (req, res, next) => {
    try {
        const reservation = await Reservation.create(req.body)
        res.status(201).json({
            success: true,
            data: reservation
        });
    } catch (err) {
        res.status(400).json({
            success: false
        })
    }
}

// @desc       Update new reservation
// @route      PUT /api/v1/reservations/:id
// @access     Private
exports.updateReservation = async (req, res, next) => {
    try {
        const reservation = await Reservation.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        });

        if (!reservation) {
            return res.status(400).json({ success: false });
        }
        res.status(200).json({ success: true, data: reservation });
    } catch (err) {
        res.status(400).json({
            success: false
        })
    }
}

// @desc       Delete reservation
// @route      DELETE /api/v1/reservations/:id
// @access     Private
exports.deleteReservation = async (req, res, next) => {
    try {
        const reservation = await Reservation.findByIdAndDelete(req.params.id);

        if (!reservation) {
            return res.status(400).json({ success: false });
        }
        res.status(200).json({ success: true, data: {} });
    } catch (err) {
        res.status(400).json({
            success: false
        })
    }
}

