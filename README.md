
Postman API Link:
https://www.getpostman.com/collections/8592f13826f35741d911

Install all dependencies with npm install
Run with npm run dev

Add a config.env in the config folder of this project with this information below

NODE_ENV=developement
PORT=5000

//Set up a mongodb database on mongodb atlas and plug the info in this var to connect it to the app
MONGO_URI=mongodb+srv://<username>:<password>@<clustername>-<mongodbserverurl.>